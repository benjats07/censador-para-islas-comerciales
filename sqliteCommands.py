#!/usr/bin/env python3
# Autor: Benjamin Torres Saavedra
# Este archivo implementa conexiones con base de datos en sqlite3 
import sqlite3
import subprocess

#import ipdb
class BD(object):
    '''Clase para realizar manipulaciones sobre una base de datos sqlite
    con los datos de los PDVs'''

    def __init__(self,archivo_bd):
        '''Constructor del objeto que ejecuta instrucciones SQL, como parametro
        recibe la ubicacion del archivo de sqlite'''
        self.archivo_bd=archivo_bd
        try:
            self.conexion=sqlite3.connect(archivo_bd, detect_types=sqlite3.PARSE_DECLTYPES)
            self.cursor=self.conexion.cursor()
        except Exception as e:
            print("Exception ",e)
            print(e)

    def kill(self,carpeta_salida1,carpeta_salida2):
        '''Metodo para eliminar el contenido de las tablas de la base de datos,ademas vacia el contenido
        de dos carpetas indicadas'''
        print("***KILL***")
        SQL='delete from Deteccion'
        self.cursor.execute(SQL)
        SQL='delete from Fotografias'
        self.cursor.execute(SQL)
        SQL='delete from Censo'
        self.cursor.execute(SQL)
        #SQL='delete from PuntosDeVenta'
        #self.cursor.execute(SQL)
        self.conexion.commit()
        subprocess.call("echo y |rm -r trueInput/*",shell=True)
        print("echo y |rm -r %s/*"%(carpeta_salida2))
        subprocess.call("echo y |rm -r %s/*"%(carpeta_salida2),shell=True)
        #subprocess.call("rm -r %s"%(carpeta_salida2),shell=True)

    def crear_pdv(self,PDV_ID):
        '''Crea el PDV con el identificador indicado en la tabla de puntos de venta'''
        SQL='INSERT INTO PuntosDeVenta (PDV_ID)\
        values (\'%s\');'
        try:
            self.cursor.execute(SQL%PDV_ID)
            self.conexion.commit()
        except Exception as e:
            print("Exception crear_pdv",e)
            print(e)

    def obtener_umbral_similitud(self,PDV_ID):
        '''Consulta en la base de datos el umbral de similitud de un punto de venta'''
        SQL='SELECT umbralSimilitud FROM PuntosDeVenta WHERE PDV_ID=%s;'
        try:
            resultado=self.cursor.execute(SQL%PDV_ID)
            return resultado.fetchall()[0][0]
        except Exception as e:
            print("Exception obtener_umbral_similitud",e)
            print(e)

    def obtener_umbral_deteccion(self,PDV_ID):
        '''Consulta en la base de datos el umbral de deteccion de un punto de venta (raiz cuadrada del tamaño de una cara)'''
        SQL='SELECT umbralDeteccion FROM PuntosDeVenta WHERE PDV_ID=%s;'
        try:
            resultado=self.cursor.execute(SQL%PDV_ID)
            print(SQL%PDV_ID)
            return resultado.fetchall()[0][0]
        except Exception as e:
            print("Exception obtener_umbral_deteccion",e)
            print(e)

    def verificar_existencia_pdv(self,PDV_ID):
        '''Consulta si un punto de venta existe en la BD'''
        SQL='SELECT PDV_ID FROM PuntosDeVenta Where PDV_ID=%s'
        try:
            resultados=self.cursor.execute(SQL%PDV_ID)
            #self.conexion.commit()
            return len(resultados.fetchall())>0
        except Exception as e:
            print("Exception verificar_existencia_pdv",e)
            print(e)

    def crear_fotografia(self,datos_dict):
        '''Crea un registro de fotografia dados los datos del punto de venta
        en un diccionario,a saber: NombreArchivo,PDV_ID,ID_camara,timestamp'''

        SQL='INSERT INTO Fotografias (NombreArchivo,PDV_ID,ID_camara,timestamp)\
        values (\'%s\',%s,%s, \'%s\');'
        try:
            self.cursor.execute(SQL%(datos_dict['nombreArchivo'],
                                        datos_dict['pdv_id'],
                                        datos_dict['camara'],
                                        datos_dict['fecha']
                                        ))
            self.conexion.commit()
        except Exception as e:
            print("Exception crear_fotografia",e)
            print(e)

    def verificar_existencia_fotografia(self,datos_dict):
        '''Consulta si una fotografia existe a partir de un diccionario con:
        PDV y numero de camara (si solo hay una camara se indica con 0)'''
        SQL='SELECT NombreArchivo FROM Fotografias WHERE NombreArchivo= \'%s\'and\
        PDV_ID=\'%s\'and ID_camara=\'%s\';'
        try:
            resultados=self.cursor.execute(SQL%(datos_dict['nombreArchivo'],
                                        datos_dict['pdv_id'],
                                        datos_dict['camara']
                                        ))
            return len(resultados.fetchall())>0
        except Exception as e:
            print("Exception verificar_existencia_fotografia",e)
            print(e)

    def crear_deteccion(self,nombreArchivo,nombreRecorte,cara,puntuacion,cuadro):
        '''Crea una deteccion en la BD, es decir, el registro de una cara
        en una fotografia. Recibe como parametros el nombre del archivo donde
        fue encontrada la cara,nombreRecorte que es el nombre de la imagen
        que contiene unicamente la cara,un objeto cara con las coordenadas
        de cada elemento que la compone y un cuadro que puede generar el recorte
        propocionado'''

        SQL='INSERT INTO Deteccion (NombreArchivo,NombreRecorte,OjoIzquierdo,OjoDerecho,Nariz,\
        OidoIzquierdo,OidoDerecho,Cuadro,Puntuacion,Genero,GrupoEdad,Atencion) \
        values (\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%s,%s,%s,%s);'
        try:
            #PROPENSO A INYECCION SQL,VER https://docs.python.org/3/library/sqlite3.html
            tmp=SQL%(nombreArchivo,nombreRecorte,
                                        str(cara.ojoIzquierdo),
                                        str(cara.ojoDerecho),
                                        str(cara.nariz),
                                        str(cara.oidoIzquierdo),
                                   		str(cara.oidoDerecho),
                                        str(cuadro),
                                        puntuacion,
                                        -1,
                                        -1,
                                        -1
                                        )
            #print(tmp)
            self.cursor.execute(tmp)
            self.conexion.commit()
        except Exception as e:
            #print("Exception crear_deteccion",e)
            #print(e)
            #VERIFICAR EXISTENCIA DE DETECCION
            pass

    def obtener_ids_pdv(self):
        '''Obtiene todos los IDs de los puntos de venta registrados'''
        SQL='SELECT PDV_ID FROM PuntosDeVenta;'
        listaPDVS=[]
        try:
            PDVS_IDS=self.cursor.execute(SQL)

            for id in PDVS_IDS:
                listaPDVS.append(id[0])

        except Exception as e:
            print("Exception obtener_ids_pdv",e)
            print(e)

        return {'lista':listaPDVS}

    def obtener_genero_deteccion(self,NombreRecorte):
        '''Dado el nombre de una deteccion consulta el genero asignado, 0=mujer, 1=hombre y 
        -1= sin asignar'''
        genero=-1
        puntuacion=-1
        SQL='SELECT genero,puntuacion from DETECCION where NombreRecorte=\'%s\';'
        try:
            resultados=self.cursor.execute(SQL%NombreRecorte)
            for resultado in resultados:
                genero=int(resultado[0])
                puntuacion=float(resultado[1])

        except Exception as e:
            print("Exception obtener_genero_deteccion ",e)
            print(SQL%NombreRecorte)
            print(e)

        return genero,puntuacion

    def obtener_grupo_edad_deteccion(self,deteccion_id):
        '''Dado el nombre de una deteccion consulta el grupo de edad asignado'''
        grupo=-1
        SQL='SELECT GrupoEdad from DETECCION where NombreRecorte=\'%s\';'
        try:
            resultados=self.cursor.execute(SQL%deteccion_id)

            for resultado in resultados:
                grupo=int(resultado[0])

        except Exception as e:
            print("Exception obtener_grupo_edad_deteccion",e)
            print(e)

        return grupo

    def obtener_rutas_detecciones_no_genero(self):
        '''Extrae de la tabla de detecciones todos los registros que no
        tengan clasifiacion de genero (i.e: genero=-1) '''

        SQL='SELECT NombreRecorte FROM Deteccion WHERE genero=-1 ORDER BY NombreArchivo;'
        listaRecortes=[]
        try:
            nombresRecortes=self.cursor.execute(SQL)

            for nombre in nombresRecortes:
                listaRecortes.append(nombre[0])#.replace(".jpg","-aligned.jpg"))

        except Exception as e:
            print("Exception ",e)
            print(e)

        return {'lista':listaRecortes}

    def obtener_rutas_detecciones_no_edad(self):
        '''Extrae de la tabla de detecciones todos los registros que no
        tengan clasifiacion de GrupoEdad (i.e: GrupoEdad=-1) '''

        SQL='SELECT NombreRecorte FROM Deteccion WHERE GrupoEdad=-1 ORDER BY NombreArchivo;'
        listaRecortes=[]
        try:
            nombresRecortes=self.cursor.execute(SQL)

            for nombre in nombresRecortes:
                listaRecortes.append(nombre[0])#.replace(".jpg","-aligned.jpg"))

        except Exception as e:
            print("Exception ",e)
            print(e)

        return {'lista':listaRecortes}

    def obtener_rutas_detecciones_no_atencion(self,PDV_ID):
        '''Extrae de la tabla de detecciones todos los registros que no
        tengan clasifiacion de atencion (i.e: atencion=-1) '''

        ###SQL='SELECT Deteccion_ID,Cuadro FROM Deteccion WHERE Atencion=-1 ORDER BY NombreArchivo;'
        SQL='SELECT Deteccion_ID,Cuadro FROM \
        (Deteccion join Fotografias on Deteccion.NombreArchivo=Fotografias.NombreArchivo) \
        WHERE Atencion=-1 and PDV_ID=%s;'
        listaRecortes = []
        listaCuadros  = []
        try:
            nombresRecortes = self.cursor.execute(SQL%PDV_ID)

            for nombre in nombresRecortes:
                listaRecortes.append(nombre[0])
                listaCuadros.append(nombre[1])

        except Exception as e:
            print("Exception ",e)
            print(e)

        return {'lista':listaRecortes,'cuadros':listaCuadros}

    def obtener_rutas_detecciones_atencion(self):
        '''Extrae de la tabla de detecciones todos los registros que SI
        tengan clasifiacion de atencion (i.e: atencion=1) '''

        SQL='SELECT Deteccion_ID,Cuadro FROM Deteccion WHERE Atencion=1 ORDER BY NombreArchivo;'
        listaRecortes = []
        listaCuadros  = []
        try:
            nombresRecortes = self.cursor.execute(SQL)

            for nombre in nombresRecortes:
                listaRecortes.append(nombre[0])
                listaCuadros.append(nombre[1])

        except Exception as e:
            print("Exception ",e)
            print(e)

        return {'lista':listaRecortes,'cuadros':listaCuadros}

    def obtener_fechas_registradas(self,PDV_ID):
        '''Dado un PDV, devuelve todas las fechas con fotografias'''
        SQL='SELECT DISTINCT date(timestamp) FROM Fotografias WHERE PDV_ID=%s;'
        fechas = []
        try:
            fechas_obtenidas = self.cursor.execute(SQL%(PDV_ID))
            for fecha in fechas_obtenidas:
                fechas.append(fecha[0])
        except Exception as e:
            print("Exception ",e)
            print(e)


        return {'lista':fechas}

    def obtener_detecciones_fecha(self,fecha,PDV_ID):
        '''Dada un identificador de punto de venta y una fecha, devuelve todas los nombres de las 
        detecciones encontradas'''
        SQL='SELECT Deteccion_ID,NombreRecorte FROM (Deteccion JOIN Fotografias on \
        Fotografias.NombreArchivo=Deteccion.NombreArchivo) JOIN PuntosDeVenta on PuntosDeVenta.PDV_ID=Fotografias.PDV_ID AND atencion=1 \
        AND date(timestamp)=\'%s\' WHERE Verificada=0 AND Fotografias.PDV_ID=%s';
            
        lista_detecciones_ID = []
        lista_nombre_recortes  = []
        try:
            nombresRecortes = self.cursor.execute(SQL%(fecha,PDV_ID))
            for nombre in nombresRecortes:
                lista_detecciones_ID.append(nombre[0])
                lista_nombre_recortes.append(nombre[1])
        except Exception as e:
            print("Exception ",e)
            print(e)

        return {'lista_IDS':lista_detecciones_ID,'lista_nombres_recortes':lista_nombre_recortes}

    def establecer_clasificacion_genero(self,archivos,resultados,probs):
        '''Establece el genero resultado[i] a un archivo[i] dentro de la
        tabla detecciones'''

        SQL='UPDATE Deteccion SET genero=\'%s\',puntuacion=%s WHERE NombreRecorte=\'%s\';'

        try:
            for clase,prob,recorte in zip(resultados,probs,archivos):
                 tmp=SQL%(clase,prob,recorte)
                 self.cursor.execute(tmp)
            self.conexion.commit()

        except Exception as e:
            print("Exception ",e)
            print(e)

    def establecer_clasificacion_edad(self,archivos,resultados,probs):
        '''Establece el GrupoEdad resultado[i] a un archivo[i] dentro de la
        tabla detecciones'''

        SQL='UPDATE Deteccion SET GrupoEdad=\'%s\' WHERE NombreRecorte=\'%s\';'

        try:
            for clase,recorte in zip(resultados,archivos):
                 tmp=SQL%(clase,recorte)
                 self.cursor.execute(tmp)
            self.conexion.commit()

        except Exception as e:
            print("Exception ",e)
            print(e)

    def establecer_clasificacion_genero_edad(self,archivos,resultados):
        '''Establece, dada la salida de AWS la clasificacion de genero y 
        edad para cada deteccion pasada en el argumento archivos'''
        resultados_genero = resultados['genero']
        resultados_edad   = resultados['edad']
        SQL='UPDATE Deteccion SET genero=%s , GrupoEdad=\'%s\' WHERE NombreRecorte=\'%s\';'

        try:
            for clase_gen,clase_edad,recorte in zip(resultados_genero,resultados_edad,archivos):
                tmp=SQL%(clase_gen,clase_edad,recorte)
                print("->",tmp)
                self.cursor.execute(tmp)
            self.conexion.commit()

        except Exception as e:
            print("Exception ",e)
            print(e)
        
    def establecer_clasificacion_individual(self,nombreCuadro,atencion):
        '''Establece la clasificacion de atencion de una unica deteccion'''
        SQL='UPDATE Deteccion SET atencion=%s WHERE NombreRecorte=\'%s\';'
        try:
            tmp=SQL%(atencion,nombreCuadro)
            #print(tmp)
            self.cursor.execute(tmp)
            self.conexion.commit()

        except Exception as e:
            print("Exception en establecer_clasificacion_individual ",e)        

    def establecer_clasificacion_atencion(self,archivos,resultados):
        '''Establece el valor de atencion resultado[i] a un archivo[i] dentro de la
        tabla detecciones'''

        SQL='UPDATE Deteccion SET Atencion=\'%s\' WHERE Deteccion_ID=\'%s\';'
        try:
            for clase,recorte in zip(resultados,archivos):
                 tmp=SQL%(clase,recorte)
                 #print(tmp)
                 self.cursor.execute(tmp)
                 self.conexion.commit()

        except Exception as e:
            print("Exception en establecer_clasificacion_atencion ",e)

    def establecer_cantidad_visitantes(self,PDV_ID,fecha,cantidad):
        '''Establece el valor de atencion resultado[i] a un archivo[i] dentro de la
        tabla detecciones'''
        SQL='SELECT Cantidad_total FROM Censo WHERE PDV_ID=\'%s\' AND timestamp=\'%s\';'
        insert=True
        try:
            tmp=SQL%(PDV_ID,fecha)
            print(tmp)
            self.conexion.commit()
            resultados=self.cursor.execute(tmp)
            self.conexion.commit()
            if len(resultados.fetchall())>0:
                insert=False #no hacemos update,hacemos insert

            print("%s censo de la fecha %s"%('INSERTANDO' if insert else 'ACTUALIZANDO',fecha))

            if(insert):
                SQL='INSERT INTO Censo (PDV_ID,timestamp,Cantidad_total)values (%s,\'%s\',%s);'
                tmp=SQL%(PDV_ID,fecha,cantidad)
                #print(tmp)
                self.cursor.execute(tmp)
            else:
                SQL='UPDATE Censo SET Cantidad_total=%s WHERE PDV_ID=\'%s\' and\
                timestamp=\'%s\';'
                tmp=SQL%(cantidad,PDV_ID,fecha)
                #print(tmp)
                self.cursor.execute(tmp)
            self.conexion.commit()
        except Exception as e:
            print("Error estableciendo cantidad visitantes-checkINSERT")
            print(e)

    def establecer_censo_genero(self,PDV_ID,fecha,censo_genero):
        '''Establece el censo de hombres y mujeres para un dia y un punto de venta concreto,
        recibe un diccionario con las clasificaciones asignadas'''
        SQL='UPDATE Censo SET Cantidad_genero= %s WHERE PDV_ID= %s and timestamp=\'%s\';'
        try:
            for genero in censo_genero.keys():
                tmp=SQL%(censo_genero[genero],PDV_ID,fecha)
                print("=>",censo_genero[genero],PDV_ID,fecha)
                if(genero==0):
                    tmp=tmp.replace('Cantidad_genero','Cantidad_mujeres')
                elif(genero==1):
                    tmp=tmp.replace('Cantidad_genero','Cantidad_hombres')
                elif(genero==2):
                    continue #El genero no fue determinado, no se contabiliza

                print(tmp)
                self.cursor.execute(tmp)
                self.conexion.commit()

        except Exception as e:
            print("Exception establecer_censo_genero ",e)
            print(e)

    def establecer_censo_edad(self,PDV_ID,fecha,censo_edad):
        '''Establece el censo de hombres y mujeres para un dia y un punto de venta concreto,
        recibe un diccionario con las clasificaciones asignadas'''
        SQL='UPDATE Censo SET Cantidad_clase_n=%s WHERE PDV_ID=%s and timestamp=\'%s\';'
        try:
            for grupo in censo_edad.keys():
                tmp=SQL%(censo_edad[grupo],PDV_ID,fecha)
                tmp=tmp.replace('Cantidad_clase_n','Cantidad_clase_'+str(grupo+1))
                #print(tmp)
                self.cursor.execute(tmp)
                self.conexion.commit()

        except Exception as e:
            print("Exception establecer_censo_edad",e)
            print(e)
