#!/usr/bin/env python3
# Autor: Benjamin Torres Saavedra
# Clase para realizar Verificacion de rostros usando descriptores de imagenes
# basado parcialmente en:
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_matcher/py_matcher.html#matcher
#import ipdb
import numpy as np
import cv2
from matplotlib import pyplot as plt

brisk = cv2.BRISK.create(thresh=10)


def extraer_descriptores(ruta_imagen):
    global brisk
    img = cv2.cvtColor(cv2.imread(ruta_imagen), cv2.COLOR_BGR2RGB)
    puntos_clave, descriptores = brisk.detectAndCompute(img, None)
    return puntos_clave, descriptores


def comparar_descriptores(puntos_descriptores1, puntos_descriptores2, umbral=0.7):

    if(puntos_descriptores1[0] == [] or puntos_descriptores2[0] == []):
        return False

    # create BFMatcher object
    respuesta = False
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(puntos_descriptores1[1], puntos_descriptores2[1])

    # Sort them in the order of their distance.
    matches = sorted(matches, key=lambda x: x.distance)

    if(len(matches) >= len(puntos_descriptores1[1])*umbral):
        respuesta = True
    else:
        respuesta = False

    return respuesta


def comparar_cara(cara1, cara2, umbral=0.7, show=False):
    '''Recibe las rutas de dos imagenes de caras para compararlas
    usando las caracteristicas BRISK'''
    respuesta = False
    # cargamos las dos imagenes a comparar
    img1 = cv2.cvtColor(cv2.imread(cara1), cv2.COLOR_BGR2RGB)  # queryImage
    img2 = cv2.cvtColor(cv2.imread(cara2), cv2.COLOR_BGR2RGB)  # trainImage

    # Initiate BRISK detector
    sift = cv2.BRISK.create(thresh=10)  # BRISK

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)

    # create BFMatcher object
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    # Match descriptors.
    matches = bf.match(des1, des2)

    # Sort them in the order of their distance.
    matches = sorted(matches, key=lambda x: x.distance)

    if(len(matches) >= len(des1)*umbral):
        # print("Corresponde")
        respuesta = True
    else:
        #print("Distinta persona")
        respuesta = False

    if(show):
        mostrar_coincidencias(img1, img2)
    return respuesta


def mostrar_coincidencias(img1, img2):
    h1, w1 = img1.shape[:2]
    h2, w2 = img2.shape[:2]

    # create empty matrix
    im = np.zeros((max(h1, h2), w1+w2, 3), np.uint8)

    # combine 2 images
    im[:h1, :w1, :3] = img1
    im[:h2, w1:w1+w2, :3] = img2

    cv2.drawMatches(img1, kp1, img2, kp2, matches[:10], outImg=im, flags=3)

    plt.imshow(im), plt.show()


def encontrar_similares(diccionario_similitudes,):
    '''Dada la similitud calculada en self.similitud, busca a todos
    indices con puntuaciones debajo de un umbral(es decir,las imagenes
    similares) y las devuelve'''

    similares = []
    for par in diccionario_similitudes.keys():
        if(diccionario_similitudes[par]):
            similares.append(par)

    return similares


def contar_personas(lista_imagenes, batch_tam, umbral):
    '''Obtiene el numero de personas total que hay en una lista de imagenes
    dado un umral de similitud'''
    lista_imagenes.reverse()
    print("****", len(lista_imagenes))
    puntos_y_desriptores = [extraer_descriptores(
        imagen) for imagen in lista_imagenes]
    # ipdb.set_trace()
    diccionario_similitudes = dict()

    n = len(puntos_y_desriptores)
    for i in range(n-1):
        for j in range(i+1, n):
            diccionario_similitudes[(i, j)] = comparar_descriptores(
                puntos_y_desriptores[i], puntos_y_desriptores[j], umbral)
    similares = len(encontrar_similares(diccionario_similitudes))
    print(similares)

    while(n-similares <= 0):
        return contar_personas(lista_imagenes, batch_tam, umbral*1.5)

    return n-similares
