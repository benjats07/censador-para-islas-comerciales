#!/usr/bin/env python3
# Autor: Benjamin Torres Saavedra
# Este archivo contiene la funcionalidad de los distintos clasificadores necesarios para el censado: genero,
# edad y atencion,ya sea usando redes neuronales, tamaños o AWS Rekognition.
#import ipdb
import math
import torch
from PIL import Image
import torch.nn as nn
import torchvision.transforms as transforms
from torchvision import models

def asignacion_clases(edad):
    '''Dada una edad, devuelve el grupo correspondiente'''
    edad=int(edad)
    if 15<=edad<=19:
        return 0
    elif 20<=edad<=24:
        return 1
    elif 25<=edad<=29 :
        return 2
    elif 30<=edad<=34 :
        return 3
    elif 35<=edad<=39 :
        return 4
    elif 40<=edad<=45 :
        return 5
    elif 46<=edad<=49 :
        return 6
    elif edad>=50:
        return 7
    else:
        return -2

def batch(iterable, n=1):
    '''Dado un objeto iterable devuelve lotes de tamaño n'''
    # '''https://stackoverflow.com/questions/8290397/how-to-split-an-iterable-in-constant-size-chunks'''
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]


def distancia(a, b):
    '''Devuelve la distancia en R2 entre a y b
    a y b son tripletas (x,y,probability)'''
    return math.sqrt(math.pow(a[0]-b[0], 2)+math.pow(a[1]-b[1], 2))


def clasificador_edad(archivo):
    '''Devuelve el clasificador de edades almacenado en el archivo pasado como parametro, se asume
    que es una red inception v3'''

    modelo_edades = models.inception_v3()
    modelo_edades.aux_logits = False
    num_ftrs = modelo_edades.fc.in_features
    modelo_edades.fc = nn.Linear(num_ftrs, 8)

    modelo_edades.load_state_dict(torch.load(archivo))
    modelo_edades.cuda()
    modelo_edades.eval()

    return modelo_edades


def clasificador_genero(archivo):
    '''Devuelve el clasificador de edades almacenado en el archivo pasado como parametro, se asume
    que es una red inception v3'''

    modelo_genero = models.inception_v3()
    modelo_genero.aux_logits = False

    num_ftrs = modelo_genero.fc.in_features
    modelo_genero.fc = nn.Linear(num_ftrs, 2)

    modelo_genero.load_state_dict(torch.load(archivo))
    modelo_genero.cuda()
    modelo_genero.eval()
    return modelo_genero


class Clasificador_AWS(object):
    '''Crea un clasificador de caras (edad y genero) que manda imagenes a AWS'''

    def __init__(self, tipo):
        '''Constructor, recibe nombre del archivo donde esta guardado el
        modelo de la red neuronal'''
        import boto3
        self.client = boto3.client('rekognition')

        self.tipo = tipo
        if(tipo == 'edad'):
            pass
        elif(tipo == 'gen'):
            pass

    @staticmethod
    def obtener_genero(registro):
        '''Recibe una parte de la respuesta de AWS Rekognition y devuelve el genero en formato 0=mujer, 1=hombre 2=Desconocido '''
        if registro == []:
            return 2
        else:
            return 0 if registro[0]['Gender']['Value'] == 'Female' else 1

    @staticmethod
    def obtener_edad(registro,tolerancia=15):
        '''Recibe una parte de la respuesta de AWS Rekognition y devuelve 
        una edad que consiste en (maximo-minimo)/2, en caso de que maximo-minimo>tolerancia
        se devuelve -2 (edad no determinada)'''

        if registro == []:
            return "-2"
        else:
            minimo = registro[0]['AgeRange']['Low']
            maximo = registro[0]['AgeRange']['High']
            diferencia = maximo - minimo 
            if (diferencia<=tolerancia):
                return int ((minimo+maximo)/2)
            else:
                return -2 #EDAD NO DETERMINADA


    @staticmethod
    def obtener_emocion(registro):
        '''Recibe una parte de la respuesta de AWS Rekognition y devuelve la emocion encontrada'''

        if registro == []:
            return "UNKNOWN"
        else:
            val = -1
            emocion = None
            for d in registro[0]['Emotions']:
                emocion_tmp = d['Type']
                confidence = d['Confidence']
                if(confidence > val):
                    emocion = emocion_tmp
                    val = confidence

        return emocion

    def predict(self, batch_tam, lista):
        '''Dados los nombres de archivos pasados como parametro, genera
        un lote que se procesara con la red neuronal.
        Devuelve una lista con las clasificaciones de cada elemento'''
        probabilidad_cara = []
        clase_genero = []
        clase_edad = []
        clase_emocion = []
        for lote in batch(lista, batch_tam):
            lote_archivos = [open(f, 'rb') for f in lote]
            resultados = [self.client.detect_faces(Image={'Bytes': f.read()}, Attributes=[
                                                   'ALL'])['FaceDetails'] for f in lote_archivos]

            generos = [Clasificador_AWS.obtener_genero(res) for res in resultados]
            # 'Female|Male'
            clase_genero.extend(generos)

            edades = [asignacion_clases(Clasificador_AWS.obtener_edad(res)) for res in resultados]
            clase_edad.extend(edades)

            emociones = [Clasificador_AWS.obtener_emocion(res) for res in resultados]
            clase_emocion.extend(emociones)
        return clase_genero, clase_edad


class Clasificador_ANN(object):
    '''Clase que cargara una red neuronal en pytorch para ejecutar
    tareas de clasificacion multiclase'''

    def __init__(self, tipo, archivoANN, transform=None):
        '''Constructor, recibe nombre del archivo donde esta guardado el
        modelo de la red neuronal, el tipo ('gen' o  edad) y una transformacion que aplicar a 
        las imagenes de entrada, si se deja vacia unicamente realiza un cambio de tamaño(224x224) y
        la normalizacion requerida para una red inception v3'''
        self.tipo = tipo
        if(tipo == 'edad'):
            self.ANN = clasificador_edad(archivoANN)
        elif(tipo == 'gen'):
            self.ANN = clasificador_genero(archivoANN)
        normalize = transforms.Normalize(
            mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

        if(transform == None):
            self.transform = transforms.Compose(
                [transforms.Resize((224, 224)), transforms.ToTensor(), normalize])
        else:
            self.transform = transform

    def predict(self, batch_tam, lista):
        '''Dados los nombres de archivos pasados como parametro (lista), genera
        un lote que se procesara con la red neuronal.
        Devuelve una lista con las clasificaciones de cada elemento'''
        probabilidad = []
        clase = []
        with torch.set_grad_enabled(False):
            for lote in batch(lista, batch_tam):
                lote_tensor = torch.Tensor(len(lote), 3, 224, 224).cuda()
                for i, archivo in enumerate(lote):
                    # print(archivo)
                    lote_tensor[i] = self.transform(
                        Image.open(archivo))  # .cuda()

                # convertimos la salida a probabilidades
                resultados = torch.softmax(self.ANN(lote_tensor), 1)
                probabilidad_lote, clase_lote = resultados.topk(1)
                probabilidad.extend(probabilidad_lote.view(-1).tolist())
                clase.extend(clase_lote.view(-1).tolist())

        return probabilidad, clase


class Clasificador_atencion(object):
    '''Clase para ejecutar tareas de clasificacion de atencion'''

    def __init__(self, tipo, umbral, PDV_ID):
        '''Constructor, recibe nombre tipo de clasificador como texto'''
        self.tipo = tipo
        self.umbral = umbral
        self.PDV = PDV_ID

    def __predict(self, lista, cuadros):
        '''Dados los nombres de archivos pasados como parametro, genera
      un lote que se procesara con la red neuronal.
      Devuelve una lista con las clasificaciones de cada elemento'''
        import re
        cuadros_parse = []
        regex_coords = '\((-*[0-9]*.[0-9]*), (-*[0-9]*.[0-9]*), (-*[0-9]*.[0-9]*), (-*[0-9]*.[0-9]*)\)'
        resultados = []
        for cuadro in cuadros:
            coincidencias = re.findall(regex_coords, cuadro)[0]
            a = (float(coincidencias[0]), float(coincidencias[1]))
            b = (float(coincidencias[2]), float(coincidencias[3]))
            h = distancia(a, b)  # hipotenusa
            area = (h**2)/2
            # print("Area=",area)
            if(area > (self.umbral**2)):
                resultados.append(1)
            else:
                resultados.append(0)

        return [], resultados

    def predict(self, lista, cuadros, batch_tam):
        return self.__predict(lista, cuadros)


if __name__ == '__main__':
    net = Clasificador_ANN('gen', 'clasificardorGenero-inception-v1.0.pt')
    lista = ['img1.jpg', 'img2.jpg', 'img3.jpg']
    predicciones = net.predict(lista)
    print(predicciones)
