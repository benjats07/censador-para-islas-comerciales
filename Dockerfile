FROM pytorch/pytorch
RUN apt-get update
RUN apt-get install -y python3 python3-pip libglib2.0-0 libsm6 libxrender1 libfontconfig1 libxext6 libxrender-dev
COPY requirements.txt	 .
RUN python3 -m pip install -r requirements.txt
COPY OpenFacePytorch ./OpenFacePytorch
COPY AlphaPose ./AlphaPose
RUN mkdir InputImages
RUN mkdir OutputFiles
COPY models ./models
COPY VerificadorCaras.py .
COPY yolo ./yolo
COPY ValidacionCaras.py .
COPY sqliteCommands.py .
COPY renombrador.py .
COPY jsonLoader.py .
COPY inputAlpha.txt .
COPY ComparadorCaras.py .
COPY clasificardorGenero-inception-v1.0.pt .
COPY clasificadorEdades-inception-v1.0.pt .
COPY Clasificador.py .
COPY BDPriceTravel.db . 
ENTRYPOINT ["/bin/bash"]
