#!/usr/bin/env python3
#Autor: Benjamin Torres Saavedra
import ipdb
import os
import sys
import math
import json
import subprocess
import datetime
import PIL
from PIL import Image
from sqliteCommands import BD
from tqdm import tqdm
from shutil import copyfile

import docker


from Clasificador import Clasificador_ANN,Clasificador_atencion,Clasificador_AWS
from VerificadorCaras import VerificadorCaras
from ComparadorCaras import *

def distancia(a,b):
    '''Devuelve la distancia en R2 entre a y b
    a y b son tripletas (x,y,probability)'''
    return math.sqrt(math.pow(a[0]-b[0],2)+math.pow(a[1]-b[1],2))

def norma(a):
    return math.sqrt(math.pow(a[0],2)+math.pow(a[1],2))

def rotar_punto(x,y,rotacion):
    x_nueva=x
    y_nueva=y
    if(rotacion<0):
        x_nueva = x*math.cos(rotacion) + y*math.sin(rotacion)
        y_nueva = x*-math.sin(rotacion) + y*math.cos(rotacion)
    elif(rotacion>0):
        x_nueva = x*math.cos(rotacion) + y*-math.sin(rotacion)
        y_nueva = x*math.sin(rotacion) + y*math.cos(rotacion)

    return x_nueva,y_nueva

def rotar_cuadro(x1,y1,x2,y2,rotacion):
    x1_n,y1_n = rotar_punto(x1,y1,rotacion)
    x2_n,y2_n = rotar_punto(x2,y2,rotacion)
    return x1_n,y1_n,x2_n,y2_n

def punto_opuesto(x_origen,y_origen,x_referencia,y_referencia,d):
    v=[x_origen-x_referencia,y_origen-y_referencia]
    norm=norma(v)
    u=[v[0]/norm,v[1]/norm]
    x_opuesto=x_origen+(d*u[0])
    y_opuesto=y_origen+(d*u[1])

    return x_opuesto,y_opuesto

class Cara(object):
    '''Clase que representa a la cara de un individuo basado en sus oidos
    y nariz'''
    def __init__(self,oidoI,oidoD,nariz,ojoI,ojoD):
        '''Constructor que usa los puntos extraidos de AlphaPose para ubicar una cara,
        oidoI,oidoD y nariz son ternas de (x,y,probability)'''
        self.oidoIzquierdo = (oidoI[0],oidoI[1],oidoI[2])
        self.oidoDerecho   = (oidoD[0],oidoD[1],oidoD[2])
        self.ojoIzquierdo  = (ojoI[0],ojoI[1],ojoI[2])
        self.ojoDerecho    = (ojoD[0],ojoD[1],ojoD[2])
        self.nariz         = (nariz[0],nariz[1],nariz[2])

    def __str__(self):
        '''Devuelve una representacion en cadena del objeto cara''' 
        #cadena="o_izq:"+str(self.ojoIzquierdo)+" o_der:"+str(self.ojoDerecho)+" nariz:"+str(self.nariz)
        cadena="oido_izq: %s oido_der: %s nariz: %s ojo_izq: %s ojo_der: %s "%(str(self.ojoIzquierdo),str(self.ojoDerecho),str(self.nariz),str(self.ojoIzquierdo),str(self.ojoDerecho))
        return cadena

    def eval(self):
        '''Asigna una probabilidad de que el objeto en la imagen sea un rostro'''
        ojos  = 0.5*(self.ojoIzquierdo[2]+self.ojoDerecho[2])
        oidos = 0.2*(self.oidoIzquierdo[2]+self.oidoDerecho[2])
        nariz = 0.3*self.nariz[2]

        self.evaluacion=oidos+ojos+nariz
        return self.evaluacion

def leer_json(nombre):
    '''Lee el archivo json pasado como parametro y devuelve su contenido en crudo como
    un arreglo'''
    f=open(nombre,'r')
    datos=json.load(f)
    return datos

def obtener_cara(joints):
    '''Dados los joints obtenidos del archivo JSON crea un objeto Cara '''
    #https://github.com/MVIG-SJTU/AlphaPose/blob/dbbcaffd588026eedd0830cae24b6c8d6f5534d2/doc/output.md
    #0,  Nose,      0,1,2
    #1,  Neck,      3,4,5
    #2,  RShoulder,  6,7,8
    #3,  RElbow,    9,10,11
    #4,  RWrist,    12,13,14
    #5,  LShoulder, 15,16,17
    #6,  LElbow,    18,19,20
    #7,  LWrist,    21,22,23
    #8,  RHip,      24,25,26
    #9,  RKnee,     27,28,29
    #10, RAnkle,    30,31,32
    #11, LHip,      33,34,35
    #12, LKnee,     36,37,38
    #13, LAnkle,    39,40,41
    #14, REye,      42,43,44
    #15, LEye,      45,46,47
    #16, REar,      48,49,50
    #17, LEar       51,52,53

    oidoIzquierdo = joints[51:]
    oidoDerecho   = joints[48:51]
    nariz         = joints[0:3]
    ojoIzquierdo  = joints[45:48]
    ojoDerecho    = joints[42:45]
    return Cara(oidoIzquierdo,oidoDerecho,nariz,ojoIzquierdo,ojoDerecho)


def calcular_cuadro(cara,tam_imagen,factor=1.5):
    '''Dado un objeto Cara devuelve    4 puntos (x1,y1,x2,y2)
    del cuadrado que encierra esa cara.
    factor es un flotante que sera usado como factor de crecimiento
    de la distancia entre oidos.
    tam_imagen SERA usado para comprobar que el cuadro no exceda la tam_imagen
     '''
    perfil=False
    xMax = tam_imagen[0]
    yMax = tam_imagen[1]
    p1=cara.oidoDerecho
    ##print("Derecho:",cara.oidoDerecho)

    p2=cara.oidoIzquierdo
    ##print("Izquierdo:",cara.oidoIzquierdo)

    ##print("Nariz;",cara.nariz)
    d_oidos=distancia(p1,p2)

    if(d_oidos<distancia(p1,cara.nariz) or d_oidos<distancia(p2,cara.nariz)):
        #print("Deteccion de perfil!")
        perfil=True
        oido_visible  = cara.oidoDerecho if cara.oidoDerecho[2]>0.8 else cara.oidoIzquierdo #nos quedamos con el oido con mayor probabilidad
        #print("Oido visible:",oido_visible)
        x_virtual = (abs(cara.nariz[0]+oido_visible[0]))/2
        y_virtual = (abs(cara.nariz[1]+oido_visible[1]))/2
        ##print("Centro virtual:",(x_virtual,y_virtual))
        d         = distancia(cara.nariz,oido_visible)
        x1 = x_virtual-d
        y1 = y_virtual-d

        x2 = x_virtual+d
        y2 = y_virtual+d

    else:
        #print("Deteccion frontal")

        d=int((d_oidos*factor)/2)

        x1 = cara.nariz[0]-d
        y1 = cara.nariz[1]-d

        x2 = cara.nariz[0]+d
        y2 = cara.nariz[1]+d

    valido=False #bandera para indicar si todas las restricciones se cumplen
    if(0<=x1,x2<xMax and 0<=y1,y2<yMax):
        valido=True

    if (not valido):
        print("\t\t RECORTE DE CARA INVALIDO")

    # deltaX=cara.ojoIzquierdo[0]-cara.ojoDerecho[0]
    # deltaY=cara.ojoIzquierdo[1]-cara.ojoDerecho[1]
    # rotacion=math.atan2(deltaX,deltaY) *180 / math.pi
    #
    #x1,y1,x2,y2=rotar_cuadro(x1,y1,x2,y2,rotacion)

    return x1,y1,x2,y2,1 if perfil else -1#,rotacion

def extraer_caras(datos):
    '''Dado los datos crudos del archivo .json de AlphaPose,
    extrae los objetos Cara de todas las peronas detectadas'''
    lista_cuerpos=datos['bodies']
    #print(lista_cuerpos[0]['joints'])
    caras=[ obtener_cara(deteccion['joints'])  for deteccion in lista_cuerpos]


    # print("CARAS:")
    # for c in caras:
    #     print(c)
    #     print(c.eval())
    #     print("   ***    ")

    for cara in list(caras):
        if(cara.eval()<.90):
            caras.remove(cara)

    return caras

def obtener_nombre_fotografia(archivos_entrada,nombreParcial):
    '''Verifica si se obtuvo informacion de un archivo con nombreParcial,
    es decir indica si hay archivos .jpg con el mismo prefijo en archivos_entrada'''
    for archivo in archivos_entrada:
        if nombreParcial in archivo:
            return archivo

    print("Archivo %s no encontrado"%nombreParcial)

def obtener_datos_nombre(nombre):
    '''Extrae los datos del nombre de la imagen, entre ellos el PDV_ID,
    la fecha de captura hasta segundos (milisegundos si el nombre es lo 
    suficientemente largo)'''
    diccionario= dict()
    try:
        guion = nombre.find('-')
        extension= nombre.find('.jpg')
        pdv_id = nombre[0:guion]
        #fecha      = nombre[guion+1:extension]
        anio =  int(nombre[guion+1:guion+5])
        mes  =  int(nombre[guion+5:guion+7])
        dia  =  int(nombre[guion+7:guion+9]) #dia de dos digitos
        hora =  int(nombre[guion+9:guion+11]) 
        minuto = int(nombre[guion+11:guion+13])
        segundo = 00 
        if(len(nombre)>19):
            segundo=int(nombre[guion+13:guion+15])
        #print(anio,mes,dia,hora,minuto)
        fecha= datetime.datetime(anio,mes,dia,hora,minuto)
        #print("Fecha:",fecha)
        diccionario={'nombreArchivo':nombre,'pdv_id':pdv_id,'fecha':fecha,'camara':0}
    except Exception as e:
        print(nombre)
        #ipdb.set_trace()

    return diccionario

def extraccionPose(carpeta_entrada,carpeta_salida,sql,tam=(256,256)):
    '''Dada una carpeta de entrada con archivos a procesar y una carpeta de salida produce
    todas las caras encontradas en imagenes individuales'''
    print("Extrayendo pose...")
    archivos_entrada=[archivo for archivo in os.listdir(carpeta_entrada)]
    #print(archivos_entrada)
    #EXTRAER DATOS DEL NOMBRE
    for archivo in archivos_entrada:
        datos=obtener_datos_nombre(archivo)
        if(not sql.verificar_existencia_pdv(datos['pdv_id'])):
            sql.crear_pdv(datos['pdv_id'])

        if(not sql.verificar_existencia_fotografia(datos)):
            sql.crear_fotografia(datos)


    #print("Buscando:",os.listdir(carpeta_salida))
    recorrido_archivos=tqdm(os.listdir(carpeta_entrada))
    for archivo in recorrido_archivos:
        #if(".json" in archivo):
        #print("B:",archivo.replace("jpg","json"))
        if(archivo.replace("jpg","json") in os.listdir(carpeta_salida)):
            recorrido_archivos.set_description(archivo)
            ruta       = carpeta_salida+archivo.replace("jpg","json")
            #print("leyendo:",ruta)
            caras      = extraer_caras(leer_json(ruta))
            #leer de carpeta_entrada/archivo
            prefijoImg = archivo#.replace("json","")
            #Obtiene el nombre del archivo de entrada
            nombreImg  = obtener_nombre_fotografia(archivos_entrada,prefijoImg)
            #print(carpeta_entrada)
            #print(nombreImg)   
            jpgfile    = Image.open(os.path.join(carpeta_entrada,nombreImg))

            #print(jpgfile.bits, jpgfile.size, jpgfile.format)
            #crear un cuadro para cara[i]
            cuadros=[calcular_cuadro(cara,jpgfile.size) for cara in caras]


            for i,cuadro in enumerate(cuadros):
                deteccion_perfil=cuadro[-1]
                cuadro=cuadro[:4]
                cuadrito=jpgfile.crop(cuadro)
                #cuadrito=cuadrito.rotate(90-rotacion)
                #carpeta_salida/faces-archivo con el nombre archivo-face_i
                cuadrito = cuadrito.resize(tam, PIL.Image.ANTIALIAS)
                #img.save(‘resized_image.jpg')
                nombreCuadro=prefijoImg.replace(".jpg","-face"+str(i)+".jpg")
                #print(nombreCuadro)
                cuadrito.save(carpeta_salida+nombreCuadro)
                sql.crear_deteccion(nombreImg,nombreCuadro,caras[i],caras[i].evaluacion,cuadro)
                if(deteccion_perfil):
                    sql.establecer_clasificacion_individual(nombreCuadro,deteccion_perfil)

def clasificacion(clasificador,carpeta_salida,sql,obtencion_elementos,actualizacion_elementos,batch):
    if(clasificador.tipo=='cercania'):
        dict_resultados=obtencion_elementos(clasificador.PDV)
    else:
        dict_resultados=obtencion_elementos()
    #print(dict_resultados)
    lista_rutas_tmp=[]
    if(len(dict_resultados['lista'])>0):
        if(clasificador.tipo == 'AWS'):
            lista_rutas_tmp=dict_resultados['lista'].copy()
            dict_resultados['lista'] = [carpeta_salida+archivo for archivo in dict_resultados['lista']]
            generos,edades= clasificador.predict(batch_tam=batch,lista=dict_resultados['lista'])
            dict_resultados['lista']=lista_rutas_tmp
            
            resultados={'genero':generos,'edad':edades}
            actualizacion_elementos(dict_resultados['lista'],resultados)

        else:
            if (clasificador.tipo in ['gen','edad']):
                lista_rutas_tmp=dict_resultados['lista'].copy()
                dict_resultados['lista'] = [carpeta_salida+archivo for archivo in dict_resultados['lista']]

            #archivos_ruta=[carpeta_salida+archivo for archivo in archivos]
            probabilidades,clases=clasificador.predict(**dict_resultados,batch_tam=batch)
            #print("probs",probabilidades)
            #print("class",clases)
            if(clasificador.tipo in ['gen','edad']):
                dict_resultados['lista']=lista_rutas_tmp
                actualizacion_elementos(dict_resultados['lista'],clases,probabilidades)
            else:
                actualizacion_elementos(dict_resultados['lista'],clases)
            print("Clasificacion completa %s"%(clasificador.tipo))
    else:
        print("Nada que clasificar,finalizando clasificacion %s"%(clasificador.tipo))

def contar_votos(lista,opciones):
    '''Dada una lista de votos y un numero de opciones disponibles,devuelve
    la opcion mas votada, en caso de haber votos con opciones negativas son 
    descartadas'''
    opciones=[0 for _ in range(opciones)]
    for voto in lista:
        if(voto>=0):
            opciones[voto]+=1

    pos=-1
    max=-1
    for i,x in enumerate(opciones):
        if(x>max):
            max=x
            pos=i
    if(max==-1):
        return -2
    return pos

def contar_votos_genero(lista,opciones):
    '''Cuenta los votos para un genero'''
    lista.sort(key=lambda deteccion:deteccion[1],reverse=True)
    return lista[0][0]

def resolver_caracteristicas_personas_similares(sql,dicc_grupos):
    '''Se encarga de decidir la edad de cada una de las personas
    dentro del dicc_grupos que le fue pasado como parametro, requiere
    conexion a la base de datos a travez del objeto sql'''

    generos_final=dict()
    edades_final=dict()
    censo_genero=dict((genero,0) for genero in range(3))
    censo_edad=dict((edad,0) for edad in range(8))


    for representante in dicc_grupos.keys():
        genero_posible  = [sql.obtener_genero_deteccion(recorte_similar) for recorte_similar in dicc_grupos[representante]]
        edades_posibles = [sql.obtener_grupo_edad_deteccion(recorte_similar) for recorte_similar in dicc_grupos[representante]]
        
        genero_posible  = [(genero,puntuacion) for genero,puntuacion in genero_posible if genero>=0]
        edades_posibles = [edad for edad in edades_posibles if edad>=0]
        #print(genero_posible)%
              
        genero_votado   = contar_votos_genero(genero_posible,2)
        edad_votada     = contar_votos(edades_posibles,8)

        generos_final[representante] = genero_votado
        censo_genero[genero_votado]+=1

        edades_final[representante]  = edad_votada
        censo_edad[edad_votada]+=1

    assert len(dicc_grupos)==(censo_genero[0]+censo_genero[1]+censo_genero[2])
    sum=0
    for i in range(8):
        sum+=censo_edad[i]
    assert len(dicc_grupos)==sum

    return generos_final,edades_final,censo_genero,censo_edad

def resolver_dia(sql,PDV_ID,fecha,dicc_dia,AWS=False):
    '''Establece el censo de un PDV_ID en una fecha indicada.Requiere
    el diccionario de personas similares de ese dia. Si AWS==True
    usara inferencia de genro y edad en la nube'''

    _,_,censo_genero,censo_edad=resolver_caracteristicas_personas_similares(sql,dicc_dia)
    sql.establecer_censo_genero(PDV_ID,fecha,censo_genero)
    sql.establecer_censo_edad(PDV_ID,fecha,censo_edad)

def procesar_imagenes_entrada(carpeta_entrada,carpeta_salida,sql):
    '''Dada la carpeta_entrada toma todas las imagenes y realiza
    extraccion de pose para cada una de ellas con AlphaPose''' 
    archivos=[]
    carpetas=[]

    print("procesando carpeta")
    for contenido in os.listdir(carpeta_entrada):
        if os.path.isdir(os.path.join(carpeta_entrada,contenido)):
            print("Agregando carpeta %s"%(contenido))
            carpetas.append(os.path.join(carpeta_entrada,contenido))
        else:
            if("jpg" in contenido or "jpg" in contenido or "png" in contenido):
                #print("Agregando archivo %s"%(contenido))
                archivos.append(os.path.join(carpeta_entrada,contenido))
    
    AlphaPose="python3 AlphaPose/demo.py --list %s --outdir %s --format cmu --save_img"%('inputAlpha.txt',sys.argv[2])

    if(len(archivos)>0):
        with open('inputAlpha.txt','w') as f:
            f.writelines("%s\n"% archivo for archivo in archivos)
        print(AlphaPose)
        subprocess.call(AlphaPose,shell=True)
        print("Terminando Alphapose")
        extraccionPose(carpeta_entrada,carpeta_salida,sql)

    for carpeta in carpetas:
        print("Procesando %s"%(carpeta))
        procesar_imagenes_entrada(carpeta,carpeta_salida,sql)

def almacenar_similares(diccionario_similares,fecha,PDV,carpeta_origen):
    '''Guarda en carpeta_origen una serie de carpetas 
    donde se almacenaran, en carpetas separadas por fecha y punto de 
    venta las personas que fueron identificadas como similares por el 
    validador''' 
    destino=os.path.join(carpeta_origen,fecha,str(PDV))

    if(os.path.exists(destino)):
        pass
    else:
        os.makedirs(destino)

    n=1 #cantidad_personas
    for representante in diccionario_similares.keys():
        destino_archivo=os.path.join(destino,str(n))
        if(os.path.exists(destino_archivo)):
            pass
        else:
            os.makedirs(destino_archivo)
        
        for similar in diccionario_similares[representante]:
            copyfile(os.path.join(carpeta_origen,similar),os.path.join(destino_archivo,similar))
        n+=1
        
def agregar_grupoEdad_a_foto(sql,carpeta_origen):
    '''OPERACION DESTRUCTIVA
    Agrega a todas los nombres de imagenes de salida el grupo de edad y
    el genero al cual fueron clasificados
    En el caso de clasificaciones validas se agrega -G# donde #
    es el grupo asignado, numeros negativos indican que no se logro
    clasificar correctamente.
    Analogamente para el genero agrega el digito 0 a mujeres, 1 a hombres 
    y -2 a personas que no pudieron ser correctamente clasificadas
    '''
    print(carpeta_origen)
    for f in os.listdir(carpeta_origen):
        f1=os.path.join(carpeta_origen,f)
        if(os.path.isdir(f1)):#fechas
            print(f1)
            for f2 in os.listdir(f1):
                f2full = os.path.join(f1,f2)
                for f3 in os.listdir(f2full):
                    print(f3)
                    f3full = os.path.join(f2full,f3)
                    for f4 in os.listdir(f3full):
                        grupo = sql.obtener_grupo_edad_deteccion(f4)
                        genero = sql.obtener_genero_deteccion(f4)[0]
                        print(f"{f4} en grupo {grupo} con genero {genero}" ) 
                        im=os.path.join(f3full,f4)
                        # print(f"reemplzando {f2} con {im}")
                        os.rename(im,im.replace(".jpg",f"-G{grupo}-{genero}.jpg"))


if __name__=='__main__':
    if(len(sys.argv)<2):
         print("Argumentos insuficientes")
         print("Ejemplo: python jsonLoader.py inputFolder/24/ examples/24/")
         sys.exit(-1)
    AWS=False
    sql=BD('BDPriceTravel.db')
    carpeta_entrada = sys.argv[1] #carpeta con las fotografias  
    carpeta_salida  = sys.argv[2]+'sep-json/' #carpeta para almacenar los resultados
    inferencia = sys.argv[3] 

    if(inferencia=='AWS'):
        print("Se usara inferencia en AWS")
        AWS=True
    elif(inferencia=='NN'):
        print("Se usara inferencia con redes neuronales")
        AWS=False
    else:
        print("Se usara inferencia en AWS")
        AWS=False

    procesar_imagenes_entrada(carpeta_entrada,carpeta_salida,sql)

    # #########################################
    # ####  COMIENZA EL CONTEO DE PERSONAS  ###
    # #########################################
    client = docker.from_env()
    #ipdb.set_trace()
    volumenes={carpeta_salida:{'bind':'/images/','mode':'rw'}}
    client.containers.run("alineador",auto_remove=True,volumes=volumenes)
    #sys.exit(-1)
    grupos_de_personas=dict()

    registro_dia_pdv=dict()

    PDVS_ID=sql.obtener_ids_pdv()['lista']
    
    for PDV in PDVS_ID:
        UMBRAL_SIMILITUD = sql.obtener_umbral_similitud(PDV)
        UMBRAL_DETECCION  = sql.obtener_umbral_deteccion(PDV)
        cercania= Clasificador_atencion('cercania',UMBRAL_DETECCION,PDV)
        print(f"Usando umbrales {UMBRAL_DETECCION} {UMBRAL_SIMILITUD} para PDV {PDV} => {cercania.umbral}")
        fechas_a_verificar=sql.obtener_fechas_registradas(PDV)['lista']#lista de fechas con fotos registradas

        for fecha in fechas_a_verificar:
            print("Consultando BD para PDV%s en la fecha %s"%(PDV,fecha))
            # #Con el clasificador de cercania discriminamos a las personas que se encuentran
            # #siendo atendidas
            clasificacion(cercania,carpeta_salida,sql,sql.obtener_rutas_detecciones_no_atencion,
                                                   sql.establecer_clasificacion_atencion,batch=100)

            detecciones=sql.obtener_detecciones_fecha(fecha,PDV)['lista_nombres_recortes']
            #rutas_recortes=[carpeta_salida+nombre.replace('.jpg','-aligned.jpg')
            #                            for nombre in detecciones['lista_nombres_recortes']]


            verificador=VerificadorCaras()
            cantidad_personas,personas_similares=verificador.contar_presonas(carpeta_salida,
                                                                            detecciones,
                                                                            batch_tam=80,
                                                                            umbral=UMBRAL_SIMILITUD)
            almacenar_similares(personas_similares,fecha,PDV,carpeta_salida)
            registro_dia_pdv[(PDV,fecha)]=personas_similares
            #print(personas_similares)
            assert cantidad_personas==len(personas_similares)
            sql.establecer_cantidad_visitantes(PDV,fecha,cantidad_personas)
        del cercania
   
    
    if(AWS):
        print("Clasificando con AWS")
        clasificadorAWS = Clasificador_AWS('AWS')
        clasificacion(clasificadorAWS,carpeta_salida,sql,sql.obtener_rutas_detecciones_no_genero,
                                                sql.establecer_clasificacion_genero_edad,batch=10)

    else:
    
        #Con el clasificador de genero asignamos etiqueta 0 a mujeres y 1 a hombres para cada
        #deteccion obtenida en el PDV

        genero=Clasificador_ANN('gen','clasificardorGenero-inception-v1.0.pt')
        clasificacion(genero,carpeta_salida,sql,sql.obtener_rutas_detecciones_no_genero,
                                                sql.establecer_clasificacion_genero,batch=100)

        #Con el clasificador de edad asignamos una clase a cada persona detectada
        edad=Clasificador_ANN('edad','clasificadorEdades-inception-v1.0.pt')
        clasificacion(edad,carpeta_salida,sql,sql.obtener_rutas_detecciones_no_edad,
                                            sql.establecer_clasificacion_edad,batch=100)
    
    print("Resolviendo censos")
    for PDV in PDVS_ID:
        fechas_a_verificar=sql.obtener_fechas_registradas(PDV)['lista']#lista de fechas con fotos registradas
        for fecha in fechas_a_verificar:
            resolver_dia(sql,PDV,fecha,registro_dia_pdv[(PDV,fecha)])

    #agregar_grupoEdad_a_foto(sql,carpeta_salida) #operacion destructiva,renombra los archivos de salida para identificar sus clasificaciones