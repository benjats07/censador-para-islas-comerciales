#!/usr/bin/env python3
# Autor: Benjamin Torres Saavedra
# Este archivo implementa funciones de Validacion de rostros (indicar si dos rostros son o no el mismo)
# basado parcialmente en:
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_matcher/py_matcher.html#matcher
import ipdb
import torch
import cv2
from torch.autograd import Variable
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components

import numpy as np
from sklearn.cluster import AffinityPropagation

from OpenFacePytorch.loadOpenFace import ReadImage, prepareOpenFace


def batch(iterable, n=1):
    # '''https://stackoverflow.com/questions/8290397/how-to-split-an-iterable-in-constant-size-chunks'''
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]


class VerificadorCaras(object):
    def __init__(self):
        '''Inicializa el modelo de red neuronal para la verificacion '''
        self.modelo = prepareOpenFace()
        self.movelo = self.modelo.eval()

    def calcular_descriptores(self, ruta_acceso, lista_detecciones, batch_tam):
        '''Calcula los descriptores de cada una de las imagenes (rutas a ellas)
        en lista_detecciones,usando un tamaño de lote de batch_tam, el
        resultado lo almacena en el campo descriptores de este objeto'''
        #ipdb.set_trace()
        # Aqui se almacenaran los descriptores
        resultados = torch.Tensor([]).cuda()
        for lote in batch(lista_detecciones, batch_tam):
            # Es mejor usar comprension de listas,pero suele haber error al generar la imagen
            # alineada y no se puede manejar la excepcion de esa manera
            #imgs=[ReadImage(ruta_acceso+imagen.replace('.jpg','-aligned.jpg'),True) for imagen in lote]
            imgs = []
            for im in lote:
                try:
                    # print(ruta_acceso+im.replace('.jpg','-aligned.jpg'))
                    imgs.append(
                        ReadImage(ruta_acceso+im.replace('.jpg', '-aligned.jpg'), True))
                except Exception as ex:
                    print("Excepcion al calcular descriptores")
                    print(im)
                    print(ex)
                    continue
            I_ = torch.cat(imgs, 0)
            I_ = Variable(I_, requires_grad=False)
            _, descriptores_lote = self.modelo(I_)
            # print(descriptores_lote)
            resultados = torch.cat((resultados, descriptores_lote))  # .cpu

        # guardamos datos en el objeto
        self.detecciones = lista_detecciones
        self.descriptores = resultados

    def obtener_similitud(self):
        '''Obtiene la similitud a partir de los descriptores para todas las
        imagenes, medida de 0 (identicos) a 4 (personas distintas),
        el resultado se almacena en el objeto'''

        descriptores = self.descriptores
        n = len(descriptores)
        evaluacion = dict()
        for i in range(n-1):
            for j in range(i+1, n):
                resta = descriptores[i]-descriptores[j]
                evaluacion[(i, j)] = torch.dot(resta, resta).item()

        self.similitud = evaluacion

    def matriz_similitud(self):
        '''Calcula la matriz de similitud de todas las imagenes haciendo uso de los resultados
        almacenados, el valor almacenado es el obtenido directamente por el validador y 
        deberia estar en el rango 0 (iguales) 4 (rostros distintos)'''
        n = len(self.descriptores)
        matriz = [0*n]*n

        for i, j in self.similitud.keys():
            matriz[i][j] = self.similitud[(i, j)]
            matriz[j][i] = self.similitud[(j, i)]

        return matriz

    def encontrar_similares(self, umbral=0.5):
        '''Dada la similitud calculada en self.similitud, busca a todos
        indices con puntuaciones debajo de un umbral(es decir,las imagenes
        similares) y las devuelve'''

        diccionario_similitudes = self.similitud
        similares = []
        for par in diccionario_similitudes.keys():
            if(diccionario_similitudes[par] <= umbral):
                similares.append(par)

        self.similares = similares

    def crear_matriz_adyacencias(self):
        '''Dados los resultados de similaridad (encontrar_similares) crea una matriz donde la 
        entrada [i,j] indica que los dos elementos estan "conectados" '''
        n = len(self.detecciones)
        matriz = [[0 for _ in range(n)] for _ in range(n)]
        for i, j in self.similares:
            matriz[i][j] = 1
            matriz[j][i] = 1

        self.matriz_adyacencia = matriz

    def agrupar_detecciones(self, tags, n_components):
        '''Devuelve un diccionario,donde cada llave es el nombre de una deteccion
        y los valores son una lista de deteccoines similares al de la llave'''

        grupos_indices = dict((i, []) for i in range(n_components))

        for i, x in enumerate(self.detecciones):
            grupos_indices[tags[i]].append(x)

        grupos_con_representante = dict()
        for k in grupos_indices.keys():
            grupos_con_representante[grupos_indices[k][0]] = grupos_indices[k]

        return grupos_con_representante

    def contar_presonas(self, ruta_acceso, lista_detecciones, batch_tam=30, umbral=0.5, Affinity=False):
        '''Calcula el numero de personas detectadas en las imagenes
        pasadas en lista_detecciones, evalua usando la red neuronal
        en lotes de batch_tam de longitud y considera iguales a personas
        con medida de similitud igual o menor a umbral.'''
        self.calcular_descriptores(ruta_acceso, lista_detecciones, batch_tam)
        self.obtener_similitud()

        if(Affinity):
            matriz_similitud = self.matriz_similitud()

            af = AffinityPropagation(affinity='precomputed', verbose=True)
            af.fit(matriz_similitud)

            if(len(matriz_similitud) == 0):
                #print("Caso raro")
                return 0, {}

            etiquetas = af.labels_
            n_components = len(np.unique(etiquetas))
            grupos = self.agrupar_detecciones(etiquetas, n_components)

        else:
            self.encontrar_similares(umbral)
            self.crear_matriz_adyacencias()
            g = csr_matrix(self.matriz_adyacencia)
            if(g.shape[1] == 0):
                #print("Caso raro")
                return 0, {}
            n_components, tags = connected_components(
                csgraph=g, directed=False, return_labels=True)
            grupos = self.agrupar_detecciones(tags, n_components)
            # num_caras=len(self.descriptores)
            # Dado que todas las personas son similares a si mismas (en una foto identica),
            # se considera que cada deteccion es una persona,despues restamos
            # las identificaciones que parecen ser las mismas para obtener el total
            # de personas.
            # personas_totales=num_caras-len(self.similares)

        return n_components, grupos


if __name__ == '__main__':
    img_paths = [	\
        # insertar aqui rutas de archivos con solo una cara para compararlos
    ]
    v = VerificadorCaras()
    cantidad_personas = v.contar_presonas(img_paths, batch_tam=30, umbral=0.8)
    print("Hay %s personas" % cantidad_personas)
