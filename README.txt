Esta carpeta contiene el programa censador.
El programa principal esta en el archivo jsonLoader.py

REQUISITOS:
-Python3.6
-GPU con al menos 4GB de memoria
-Cuda 10.2 (posiblemente funcione desde cuda 10,no fue probado)
-Instalacion de los requisitos en python3 usando:
	python3 -m pip install requierements.txt

	los cuales incluyen:
	pytorch
	torchvision
	visdom
	nibabel
	pandas
	tqdm
	matplotlib==3.0
	opencv-python
	scikit-learn
	tqdm
	docker	
	botocore
	boto3
	docker-pycreds
	ipdb
	numpy
	opencv-python
	pandas

USO:

Para la ejecucion se usa la siguiente instruccion:
	python3 jsonLoader [ruta_imagenes_entrada] [ruta_archivos_salida] [AWS/NN]

Ambas rutas deben ser absolutas, para lo cual es valido utilizar $PWD en caso de estar 
dentro de esta misma carpeta.

El tercer argumento indica si se usara inferencia en AWS o haciendo uso de redes neuronales,
es importante configurar las credenciales de acceso correctas en el sistema destino.

En lo que respecta a las imagenes de entrada, estas pueden estar organizadas dentro de 
carpetas (cuyo nombre es irrelevante) para mantener la organizacion de los archivos. Las
imagenes deben cumplir con el siguiente formato de nombre:

[PDV-ID]-[anio a 4 digitos][mes a dos digitos][dia a dos digitos][hora en formato 24horas].jpg

Ejemplo:
Para el punto de venta 10 y para fotografias tomadas el dia 10 de noviembre del 2018 son validos
los siguientes nombres y la estructura:

.
── carpeta-fotografias
   ├── 10-201811101200.jpg
   ├── 10-2018111018431626.jpg

[Es importante verificar que en la carpeta unicamente se encuentren archivos .jpg]

Los resultados quedaran registrados en la base de datos de sqlite3 BDPriceTravel.bd.
En la tabla PuntosDeVenta queda registrado cada punto de venta encontrado (inferido por sus 
fotografias encontradas). En Fotografias se almacena cada una de las rutas de los archivos
encontrados. En Detecciones se registran datos sobre cada uno de los recortes de caras que
fueron encontradas en las fotografias.Finalmente en Censo se almacena el resultado
de evaluar y contabilizar cada uno de los rostros que fue considerado importante.

En la tabla PuntosDeVenta se guardan dos datos importantes:
UmbralSimilitud, que controla cuanto deben parecerse dos rostros para ser considerados 
iguales por el programa. Debe estar en el rango [0-4], mientras mas bajo el valor, mas
esticto el proceso. Ademas esta el valor UmbralDeteccion, que se encarga de controlar
que tamaño de cara es necesario para que sea contabilizada la persona. Este valor
corresponde a el tamaño en pixeles del borde de un cuadro que encierra al rostro 
importante. Este dato es importante debido a que es dependiente de cada punto de venta,
del tamaño de la imagen tomada por la camara y potencialmente de su ubicacion.Ambos valores
se establecen por defecto en 0.7 y 45(para imagenes de S3), respectivamente,aunque pueden establecerse
manualmente antes de iniciar la ejecucion.


