#import ipdb
import torch
import torch.nn as nn
from PIL import Image
import torchvision.transforms as transforms

import imageio
import numpy as np
import scipy.misc

from MobileFaceNet_Pytorch.core import model

def similitud(v1,v2):
    '''Distancia en Rn'''
    resultado=-1
    if(len(v1)==len(v2)):
        resta=v1-v2
        resta=resta*resta
        resta=resta.sum()
        distancia=resta.sqrt()

    return resultado


class Validador(object):
    '''Objeto que se encarga de validar si dos rostros son de la misma persona
    haciendo uso del validador MobileFaceNet'''

    def __init__(self,ruta_modelo):
        '''Constructor del modelo,recibe ruta a donde se encuentra almacenado
        el modelo de verificacion'''

        self.net=model.MobileFacenet()
        self.net.load_state_dict(torch.load(ruta_modelo))
        self.net.eval()
        self.transform=transforms.Compose([transforms.Resize((112,96)),transforms.ToTensor()])

    def verificar(self,img1,img2):
        '''Devuelve la medida de similitud entre img2 e img2'''

        im1=self.transform(Image.fromarray(imageio.imread(img1).transpose(2,0,1).astype('uint8')), 'RGB')
        im2=self.transform(Image.fromarray(imageio.imread(img2).transpose(2,0,1).astype('uint8')), 'RGB')
        #im1.transpose(2,0,1)
        #imglist=[im1,im2]
        i = torch.FloatTensor(2, 3, 112, 96)
        i[0]=(im1-127.5)/128.0
        i[1]=(im2-127.5)/128.0
        #ipdb.set_trace()
        #imglist[i] = (imglist[i] - 127.5) / 128.0
        #imglist[i] = imglist[i].transpose(2, 0, 1)

        with torch.set_grad_enabled(False):
            resultados=self.net(i)

        print(resultados[0])
        print(len(resultados[0]))
        print(len(resultados[1]))
        return similitud(resultados[0],resultados[1])


if __name__=='__main__':
    net=Validador('/home/benja/PriceTravel/AlphaPose/MobileFaceNet_Pytorch/modeloVerificacion.pt')
    a="/home/benja/Escritorio/carasValidacion/39-201801011525.face1.jpg"
    b="/home/benja/Escritorio/carasValidacion/39-201801031240.face1.jpg"
    print(net.verificar(a,b))
